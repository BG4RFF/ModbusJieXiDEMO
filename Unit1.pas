unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, IdBaseComponent, IdComponent, IdTCPConnection, IdTCPClient,uHashTable,
  ExtCtrls, SPComm, Grids, StdCtrls, ComCtrls;

type
  TForm1 = class(TForm)
    pn1: TPanel;
    lbl8: TLabel;
    lbl9: TLabel;
    lbl10: TLabel;
    btnScanStart: TButton;
    edtCheckRote: TEdit;
    edtBeginAddress: TEdit;
    edtAddressLen: TEdit;
    btnScanStop: TButton;
    cm1: TComm;
    tmr1: TTimer;
    tcp1: TIdTCPClient;
    pn3: TPanel;
    grp2: TGroupBox;
    mmo1: TMemo;
    grp3: TGroupBox;
    mmo2: TMemo;
    pn2: TPanel;
    pgc1: TPageControl;
    ts1: TTabSheet;
    grp1: TGroupBox;
    lbl1: TLabel;
    lbl2: TLabel;
    lbl3: TLabel;
    lbl4: TLabel;
    lbl5: TLabel;
    btnSwitch: TButton;
    cbb1: TComboBox;
    cbb2: TComboBox;
    cbb3: TComboBox;
    cbb4: TComboBox;
    cbb5: TComboBox;
    ts2: TTabSheet;
    lb1: TLabel;
    lb2: TLabel;
    edtIP: TEdit;
    edtPort: TEdit;
    pn4: TPanel;
    pn5: TPanel;
    lb3: TLabel;
    edtConType: TEdit;
    lb4: TLabel;
    imgOn: TImage;
    imgOff: TImage;
    btnWrite: TButton;
    gridData: TStringGrid;
    lb5: TLabel;
    procedure btnSwitchClick(Sender: TObject);
    procedure GetListComPorts(Ports: TStrings);
    procedure aInit;
    procedure FormCreate(Sender: TObject);
    procedure edtBeginAddressExit(Sender: TObject);
    procedure edtCheckRoteExit(Sender: TObject);
    procedure btnScanStartClick(Sender: TObject);
    procedure btnScanStopClick(Sender: TObject);
    procedure tmr1Timer(Sender: TObject);
    procedure cm1ReceiveData(Sender: TObject; Buffer: Pointer;
      BufferLength: Word);
    procedure ResGrid;
    procedure TranReceiveData(str:string);
    procedure TranReceiveDataTCP(str:string);
    function SendString(sMsg:string):Boolean;
    function CalCRC16(strData:String):string;
    procedure FormShow(Sender: TObject);
    procedure pgc1Change(Sender: TObject);
    procedure edtPortExit(Sender: TObject);
    procedure gridDataSelectCell(Sender: TObject; ACol, ARow: Integer;
      var CanSelect: Boolean);
    procedure btnWriteClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    ReadValueList:THashTable;
    snID:integer;
  end;

var
  Form1: TForm1;
  td:Dword;
  doRead:Boolean;
  iBeginAddress,iAddressLen:integer;
  function HexStrToStr(const S:string):string;
  function StrToHexStr(const S,split:string):string;

implementation

{$R *.dfm}
procedure ReadThread;
var
  s: string;
  sHex:string;
  iCnt:integer;
  str:string;
begin
  while True do
  begin
    try
    if doRead and form1.tcp1.Connected then
    begin
      try
      s:=Form1.tcp1.CurrentReadBuffer;
      sHex:=StrToHexStr(s,'');
      icnt:=Trunc(Length(sHex)/2);
      form1.mmo2.Lines.Add('('+IntToStr(iCnt)+')'+sHex);
      form1.TranReceiveDataTCP(sHex);
      form1.ResGrid;
      except on E:Exception do
      begin
        if Pos('Socket Error # 10054',E.Message)>0 then
        begin
          form1.mmo2.Lines.Add('接收-连接中断，重试中！');
          if form1.tcp1.Connected then
            form1.tcp1.Disconnect;
          form1.tcp1.Connect(1000);
        end
        else
        begin
          form1.mmo2.Lines.Add('接收-连接异常（'+E.Message+'），重试中！');
          if form1.tcp1.Connected then
            form1.tcp1.Disconnect;
          form1.tcp1.Connect(1000);
        end;
      end;
      end;
    end;
    Except
    end;
    sleep(50);
  end;
end;
procedure TForm1.GetListComPorts(Ports: TStrings);
var
  KeyHandle: HKEY;
  ErrCode, Index: Integer;
  ValueName, Data: string;
  ValueLen, DataLen, ValueType: DWORD;
  TmpPorts: TStringList;
begin
  ErrCode := RegOpenKeyEx(
    HKEY_LOCAL_MACHINE,
    'HARDWARE\DEVICEMAP\SERIALCOMM',
    0,
    KEY_READ,
    KeyHandle);

  if ErrCode <> ERROR_SUCCESS then
    Exit;  // raise EComPort.Create(CError_RegError, ErrCode);

  TmpPorts := TStringList.Create;
  try
    Index := 0;
    repeat
      ValueLen := 256;
      DataLen := 256;
      SetLength(ValueName, ValueLen);
      SetLength(Data, DataLen);
      ErrCode := RegEnumValue(
        KeyHandle,
        Index,
        PChar(ValueName),
        Cardinal(ValueLen),
        nil,
        @ValueType,
        PByte(PChar(Data)),
        @DataLen);

      if ErrCode = ERROR_SUCCESS then
      begin
        SetLength(Data, DataLen);
        TmpPorts.Add(Data);
        Inc(Index);
      end
      else
        if ErrCode <> ERROR_NO_MORE_ITEMS then
          exit; //raise EComPort.Create(CError_RegError, ErrCode);

    until (ErrCode <> ERROR_SUCCESS) ;

    TmpPorts.Sort;
    Ports.Assign(TmpPorts);
  finally
    RegCloseKey(KeyHandle);
    TmpPorts.Free;
  end;
end;

procedure TForm1.btnSwitchClick(Sender: TObject);
var
  BaudRate :integer;
begin
  if btnSwitch.Caption = '打开串口' then
  begin
    if not TryStrToInt(cbb2.Text,BaudRate) then
       begin
         cbb2.SetFocus;
         Raise exception.Create('波特率设定有误! 请重新输入');
       end;

    //设置串口参数**************************Begin
    //端口
    cm1.CommName:=cbb1.Text;
    //波特率
    if cbb2.Text = 'Custom' then
      begin
        cbb2.Style := csDropDown;
        cbb2.SetFocus;
      end
    else begin
      if  cbb2.ItemIndex >0 then
        cbb2.Style := csDropDownList;
      if TryStrToInt(cbb2.Text,BaudRate) then
             cm1.BaudRate := BaudRate;
    end;
    //校验位TParity = ( None, Odd, Even, Mark, Space );
    cm1.Parity := TParity(cbb3.ItemIndex);
    //数据位TByteSize = ( _5, _6, _7, _8 );
    cm1.ByteSize :=  TByteSize(cbb4.ItemIndex);
    //停止位TStopBits = ( _1, _1_5, _2 );
    cm1.StopBits := TStopBits(cbb5.ItemIndex);
    //设置串口参数**************************End

    cm1.StartComm;
    btnSwitch.Caption := '关闭串口';
    cbb1.Enabled := false;
    cbb2.Enabled := false;
    cbb3.Enabled := false;
    cbb4.Enabled := false;
    cbb5.Enabled := false;
    //btnSend.Enabled   := true;
  end
  else //if Button1.Caption = '关闭串口' then
    begin
    cm1.StopComm;
    btnSwitch.Caption := '打开串口';
    cbb1.Enabled := true;
    cbb2.Enabled := true;
    cbb3.Enabled := true;
    cbb4.Enabled := true;
    cbb5.Enabled := true;
    //btnSend.Enabled   := false;
    end;
end;

procedure TForm1.aInit;
begin
  ReadValueList:=THashTable.Create();
  GetListComPorts(cbb1.Items);    //得到串口列表
  cbb1.ItemIndex := 0;
  cm1.CommName := cbb1.Text;
  cbb2.ItemIndex := 6;
  cm1.BaudRate := StrToInt(cbb2.Text);
  cbb3.ItemIndex := 0;
  cm1.Parity := None;
  cbb4.ItemIndex := 3;
  cm1.ByteSize := _8;
  cbb5.ItemIndex := 0;
  cm1.StopBits := _1;
end;

procedure TForm1.FormCreate(Sender: TObject);
begin
  aInit;
end;

procedure TForm1.edtBeginAddressExit(Sender: TObject);
begin
  TEdit(Sender).Text:=IntToStr(StrToIntDef(TEdit(Sender).Text,1));
end;

procedure TForm1.edtCheckRoteExit(Sender: TObject);
begin
  TEdit(Sender).Text:=IntToStr(StrToIntDef(TEdit(Sender).Text,300));
end;

function HexStrToStr(const S: string): string;
var
  t:Integer;
  ts:string;
  M,Code:Integer;
begin
  //16进制字符串转换成字符串
  t:=1;
  Result:='';
  while t<=Length(S) do
  begin   //xlh 2006.10.21
    while (t<=Length(S)) and (not (S[t] in ['0'..'9','A'..'F','a'..'f'])) do
      inc(t);
    if (t+1>Length(S))or(not (S[t+1] in ['0'..'9','A'..'F','a'..'f'])) then
      ts:='$'+S[t]
    else
      ts:='$'+S[t]+S[t+1];
    Val(ts,M,Code);
    if Code=0 then
      Result:=Result+Chr(M);
    inc(t,2);
  end;
end;

function StrToHexStr(const S,split:string):string;
var
  I:Integer;
begin
  //字符串转换成16进制字符串
  for I:=1 to Length(S) do
  begin
    if I=1 then
      Result:=IntToHex(Ord(S[1]),2)
    else
      Result:=Result+split+IntToHex(Ord(S[I]),2);
  end;
end;

procedure TForm1.ResGrid;
var
  icnt,iAdds:integer;
begin
  for icnt:=1 to gridData.RowCount-1 do
  begin
    iAdds:=strtointdef(gridData.cells[0,icnt],-1);
    if iAdds>=0 then
      gridData.Cells[1,icnt]:=IntToStr(ReadValueList.Get(IntToStr(iAdds)))
    else
      gridData.Cells[1,icnt]:='';
  end;
end;

procedure TForm1.TranReceiveData(str: string);
var
  iBeginAddress,iAddressLen:integer;
  iAdd,iLen,iTemp,icnt:integer;
begin
  if Length(str)<8 then
    exit;
  if Copy(str,3,2)<>'03' then
    exit;
  iLen:=Trunc(StrToInt('$'+Copy(str,5,2))/2);
  iBeginAddress:=StrToInt(edtBeginAddress.Text);
  ReadValueList.Clear;
  iAdd:=iBeginAddress;
  for icnt:=0 to iLen-1 do
  begin
    iTemp:=StrToInt('$'+Copy(str,7+icnt*4,4));
    ReadValueList.Put(IntToStr(iAdd),iTemp);
    Inc(iAdd);
  end;
end;

procedure TForm1.TranReceiveDataTCP(str: string);
var
  iBeginAddress,iAddressLen:integer;
  iAdd,iLen,iTemp,icnt:integer;
begin
  if Length(str)<12+8 then
    exit;
  if Copy(str,12+3,2)<>'03' then
    exit;
  iLen:=Trunc(StrToInt('$'+Copy(str,12+5,2))/2);
  iBeginAddress:=StrToInt(edtBeginAddress.Text);
  ReadValueList.Clear;
  iAdd:=iBeginAddress;
  for icnt:=0 to iLen-1 do
  begin
    iTemp:=StrToInt('$'+Copy(str,12+7+icnt*4,4));
    ReadValueList.Put(IntToStr(iAdd),iTemp);
    Inc(iAdd);
  end;
end;

procedure TForm1.cm1ReceiveData(Sender: TObject; Buffer: Pointer;
  BufferLength: Word);
var
  str :string;
  strHex,strOrig,strCheck:string;
begin
  //串口接收事件
  try
  SetLength(Str,BufferLength);
  move(buffer^,pchar(@Str[1])^,bufferlength);
  if length(str)>4 then
  begin
    strHex:=StrToHexStr(Str,'');
    strOrig:=Copy(strHex,1,Length(strHex)-4);
    strCheck:=Copy(strHex,Length(strHex)-4+1,4);
    mmo2.Lines.Add(StrToHexStr(Str,''));
    if CalCRC16(strOrig)=strCheck then     //校验通过才处理
    begin
      TranReceiveData(strHex);
      ResGrid;
    end;
  end;
  except
  end;
end;

function TForm1.SendString(sMsg: string): Boolean;
begin
  //通过串口发送数据
  cm1.WriteCommData(Pchar(sMsg),Length(sMsg));
end;


//******************************************************
// CalCRC16用于计算Modbus RTU的CRC16
// 多项式公式为X16+X15+X2+1
//******************************************************
function TForm1.CalCRC16(strData:String):string;
const
  GENP=$A001;  //多项式公式X16+X15+X2+1（1100 0000 0000 0101）
var
  crc:Word;
  i:Integer;
  j:Integer;
  iCount:integer;
  tmp:Byte;
  AData:array[0..255] of Byte;
  AStart,AEnd:Integer;
begin
  result:='';

  iCount:=Trunc(Length(strData)/2); //读入需要计算的字符串长度
  i:=1;
  j:=0;
  for j:=0 to iCount-1 do
  begin
    AData[j]:=StrToInt('$'+copy(strData,1+2*j,2));
    //    if (i mod 2)=0 then    //每2个字符放入一个字节中
    //      i:=i+1;
    //    if i>=Length(strData) then
    //      Break;
    //    AData[j]:=StrToInt('$'+copy(strData,i,2)); //取出字符并转换为16进制数
    //    i:=i+1;
  end;

  crc:=$FFFF;                //将余数设定为FFFF
  for i:=0 to iCount-1 do   //对每一个字节进行校验
  begin
    crc:=crc xor AData[i];      //将数据与CRC寄存器的低8位进行异或
    for j:=0 to 7 do         //对每一位进行校验
    begin
      tmp:=crc and 1;        //取出最低位
      crc:=crc shr 1;        //寄存器向右移一位
      crc:=crc and $7FFF;    //将最高位置0
      if tmp=1 then          //检测移出的位，如果为1，那么与多项式异或
        crc:=crc xor GENP;
      crc:=crc and $FFFF;
    end;
  end;
  Result:=IntToHex(swap(crc),4);
end;
procedure TForm1.btnScanStopClick(Sender: TObject);
begin
  pgc1.Enabled:=true;
  if edtConType.Text='串口' then
  begin
    btnSwitchClick(nil);
  end
  else
  begin
    doRead:=false;
    Sleep(100);
    mmo2.Lines.Add('disConnected from server');
    if tcp1.Connected then
      tcp1.Disconnect;
  end;
  tmr1.Enabled:=false;

  imgOff.Visible  := not tmr1.Enabled;
  imgOn.Visible   :=tmr1.Enabled;
  edtBeginAddress.Enabled:=not tmr1.Enabled;
  edtAddressLen.Enabled:=not tmr1.Enabled;
  edtCheckRote.Enabled:=not tmr1.Enabled;
  btnScanStart.Enabled:=not tmr1.Enabled;
  btnScanStop.Enabled:=tmr1.Enabled;
  btnWrite.Enabled:=tmr1.Enabled;
end;

procedure TForm1.btnScanStartClick(Sender: TObject);
var
  icnt:integer;
begin

  iBeginAddress:=StrToInt(edtBeginAddress.Text);
  iAddressLen:=StrToInt(edtAddressLen.Text);
  if iAddressLen>127 then
    Raise Exception.Create('长度不能大于127');
  ReadValueList.Clear;
  gridData.RowCount:=iAddressLen+1;
  gridData.Cells[0,0]:='地址';
  gridData.Cells[1,0]:='读取值';
  gridData.Cells[2,0]:='待写入值';
  for icnt:=0 to iAddressLen-1 do
  begin
    ReadValueList.Put(IntToStr(iBeginAddress+icnt),0);
    gridData.Cells[0,icnt+1]:=IntToStr(iBeginAddress+icnt);
    gridData.Cells[1,icnt+1]:='';
  end;
  tmr1.Interval:=StrToIntDef(edtCheckRote.Text,300);

  if edtConType.Text='串口' then
  begin
    try
    btnSwitchClick(nil);
    except on E:Exception do
      Raise Exception.Create('Modbus-RTU 连接失败！'+E.Message);
    end;
  end
  else
  begin
    tcp1.Host:=edtip.Text;
    tcp1.Port:=StrToIntDef(edtPort.Text,502);
    if tcp1.Connected then
      tcp1.Disconnect;
    try
    tcp1.Connect(2000);
    except on E:Exception do
      Raise Exception.Create('Modbus-TCP 连接失败！'+E.Message);
    end;
    doRead:=true;
  end;
  tmr1.Enabled:=True;
  pgc1.Enabled:=false;

  imgOff.Visible  := not tmr1.Enabled;
  imgOn.Visible   :=tmr1.Enabled;
  edtBeginAddress.Enabled:=not tmr1.Enabled;
  edtAddressLen.Enabled:=not tmr1.Enabled;
  edtCheckRote.Enabled:=not tmr1.Enabled;
  btnScanStart.Enabled:=not tmr1.Enabled;
  btnScanStop.Enabled:=tmr1.Enabled;
  btnWrite.Enabled:=tmr1.Enabled;
end;

procedure TForm1.tmr1Timer(Sender: TObject);
var
  sTcpHead,sData,sCmd,sCRC:string;
begin
  sData:='0103'
       +IntToHex(iBeginAddress,4)
       +IntToHex(iAddressLen,4);
  sCRC:=CalCRC16(sData);
  sTcpHead:=IntToHex(snID,2)
       +'00000000'
       +IntToHex(trunc(Length(sData)/2),2);

  tmr1.Enabled:=false;
  if edtConType.Text='串口' then
  begin
    if not imgOn.Visible then
    begin
      tmr1.Enabled:=false;
      btnScanStop.Click;
      exit;
    end;
    sCmd:=sData+CalCRC16(sData);
    mmo1.Lines.Add(sCmd);
    SendString(HexStrToStr(sCmd));
  end
  else
  begin
    sCmd:=sTcpHead+sData;
    mmo1.Lines.Add(sCmd);
    try
    try
    tcp1.Write(HexStrToStr(sCmd));
    except on E:Exception do
    begin
        if Pos('Socket Error # 10054',E.Message)>0 then
        begin
          form1.mmo2.Lines.Add('发送-连接中断，重试中！');
          if form1.tcp1.Connected then
              form1.tcp1.Disconnect;
          form1.tcp1.Connect(1000);
        end
        else
        begin
          form1.mmo2.Lines.Add('发送-连接异常（'+E.Message+'），重试中！');
          if form1.tcp1.Connected then
              form1.tcp1.Disconnect;
          form1.tcp1.Connect(1000);
        end;
    end;
    end;
    except
    end;
    Inc(snID);
    if snID>9 then
      snID:=0;
  end;
  tmr1.Enabled:=true;
end;

procedure TForm1.FormShow(Sender: TObject);
begin
  pgc1Change(nil);
  CreateThread(nil,0,@ReadThread,nil,0,td);
end;

procedure TForm1.pgc1Change(Sender: TObject);
begin
  pn4.Caption:='通讯方式：'+pgc1.ActivePage.Caption;
  edtConType.Text:=pgc1.ActivePage.Caption;
end;

procedure TForm1.edtPortExit(Sender: TObject);
begin
  TEdit(Sender).Text:=IntToStr(StrToIntDef(TEdit(Sender).Text,502));
end;

procedure TForm1.gridDataSelectCell(Sender: TObject; ACol, ARow: Integer;
  var CanSelect: Boolean);
begin
  if ACol=2 then
    gridData.Options:=gridData.Options+[goEditing]
  else
    gridData.Options:=gridData.Options-[goEditing];
end;

procedure TForm1.btnWriteClick(Sender: TObject);
var
  sTcpHead,sData,sCmd,sCRC:string;
  icnt,iValue,iValueSet:integer;
  sAddress:string;
begin
  sData:='';
  for icnt:=1 to gridData.RowCount-1 do
  begin
    sAddress:=gridData.Cells[0,icnt];
    iValue:=ReadValueList.Get(sAddress);
    if gridData.Cells[2,icnt]<>'' then
      iValue:=StrToIntDef(gridData.Cells[2,icnt],0);
    sData:=sData+inttohex(iValue,4);
  end;
  sData:='0110'
       +IntToHex(iBeginAddress,4)
       +IntToHex(iAddressLen,4)
       +IntToHex(iAddressLen*2,2)
       +sData;
  sCRC:=CalCRC16(sData);
  sTcpHead:=IntToHex(snID,2)
       +'00000000'
       +IntToHex(trunc(Length(sData)/2),2);

  tmr1.Enabled:=false;
  if edtConType.Text='串口' then
  begin
    sCmd:=sData+CalCRC16(sData);
    mmo1.Lines.Add(sCmd);
    SendString(HexStrToStr(sCmd));
  end
  else
  begin
    sCmd:=sTcpHead+sData;
    mmo1.Lines.Add(sCmd);
    try
    try
    tcp1.Write(HexStrToStr(sCmd));
    except on E:Exception do
    begin
        if Pos('Socket Error # 10054',E.Message)>0 then
        begin
          form1.mmo2.Lines.Add('发送-连接中断，重试中！');
          if form1.tcp1.Connected then
              form1.tcp1.Disconnect;
          form1.tcp1.Connect(1000);
        end
        else
        begin
          form1.mmo2.Lines.Add('发送-连接异常（'+E.Message+'），重试中！');
          if form1.tcp1.Connected then
              form1.tcp1.Disconnect;
          form1.tcp1.Connect(1000);
        end;
    end;
    end;
    except
    end;
    Inc(snID);
    if snID>9 then
      snID:=0;
  end;
  tmr1.Enabled:=true;
end;

end.
