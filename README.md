# Modbus解析DEMO
不借助其它底层Modbus控件，直接操作底层通讯代码，实现Modbus读写“保持寄存器”数据，支持串口通讯、TCP通讯两种方式。代码只作演示，可做进一步封装，也可稍作修改，用于非标准的Modbus通讯。本代码使用了一个第三方控件SPComm，开发测试环境为Delphi7+Win7，我一直想摆脱Modbus控件(MBAXP Modbus Master ActiveX Control
)，最近自己开发了这个DEMO，分享给大家。